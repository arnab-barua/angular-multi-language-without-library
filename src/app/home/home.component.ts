import { TranslateService } from './../services/translate.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private translate : TranslateService,private route : Router) { 

    translate.use('eng');
    translate.load(['common','user']);
  }

  ngOnInit() {
    // this.translate.get('common_ita.datetime').subscribe(a => console.log(a));
  }


}
