import { UsersComponent } from './users/users.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';


const routes: Routes = [
  { path : '', component : HomeComponent, pathMatch : 'full' },
  { path : 'home', component : HomeComponent, pathMatch : 'full' },
  { path : 'users', component : UsersComponent, pathMatch : 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
