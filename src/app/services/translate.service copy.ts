import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { of, Observable } from 'rxjs';
import { map, take, catchError, publishReplay, refCount } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TranslateService {
  private language : string = "eng";
  private languageFiles : string[] = [];

  translate : any = {};

  constructor(private http : HttpClient) { }

  use(lang : string) {
    this.language = lang.toLowerCase();
    this.load(this.languageFiles);
  }

  load(files : string[]) {
    this.languageFiles = files;
    this.languageFiles.forEach(lang => {
      this.loadLanguageJsonFile(lang).subscribe(res => this.translate[lang] = res);
    })
  }

  private loadLanguageJsonFile(lang : string) {
    return this.http.get<any>(`assets/localization/${lang}_${this.language}.json`);
  }

  get(key : string, params : any) : Observable<any> {
    var keyParts = key.split('.');
    var langFile = keyParts[0];
    if(this.translate[langFile] == undefined) {
      return this.loadLanguageJsonFile(keyParts[0])
      .pipe(
        map(
          res => {
            this.translate[langFile] = res;
            var dt = this.extractValueFromKey(keyParts);
            return(this.replcaeWithParam(dt, params));
          }
        ),
        publishReplay(1), // this tells Rx to cache the latest emitted
        refCount()
      )          
    }
    else {
      var dt = this.extractValueFromKey(keyParts);
      return of(this.replcaeWithParam(dt, params));
    }
  }

  private extractValueFromKey(keyParts : string[]) {
    var tempData = this.translate;
    keyParts.forEach(a => {
      tempData = tempData[a];
    });
    return tempData ? tempData : keyParts.join('.');
  }

  private replcaeWithParam(source : any, params : any) {
    if(typeof source == "string") {
      var sourceString : string = source;
      for(var k in params) {   
        sourceString = sourceString.replace('{{'+k+'}}', params[k]);
      }
      return sourceString;
    }
    return source;
  }
}
